Lecturas
========

Aquí tenemos la lista de lecturas que son necesarias de revisar por nuestros estudiantes en el Programa de Capacitadero.

---

Lecturas-01
-----------
[Ideas para Startups](01-Inicios/ideas-para-startups.txt), Paul Graham.

[Consejos para ambiciosos de 19 años](01-Inicios/Consejos-para-ambiciosos-de-19.txt), Sam Altman.

[Buenas y malas razones para convertirse en emprendedor](01-Inicios/buenas-malas-razones-emprendedor.txt), Dustin Moskovitz.

Lecturas-02
-----------
[Como son en realidad las Startups](02-Startups/como-son-en-realidad-las-startups.txt), Paul Graham.

[Startups en 13 frases](02-Startups/startups-en-13-frases.txt), Paul Graham.

[Discurso de graduación en Harvard 2017 ](02-Startups/discurso-zuckerberg-harvard-2017.txt), Mark Zuckerberg.

Lecturas-03
-----------
[Cómo hacer preguntas de manera inteligente?](03-Hackers/preguntas-de-manera-inteligente.txt), Eric Raymond.

[Hackers y Pintores](03-Hackers/hackers-y-pintores.txt), Paul Graham.


Autores
=======

Lista de autores que componen los textos leidos en el Programa.

---

[Paul Graham](http://www.paulgraham.com/):

Programador, escritor e inversor. En 1995, él y Robert Morris comenzaron Viaweb, el primer software como compañía de servicios. 

Viaweb fue adquirido por Yahoo en 1998, donde se convirtió en Yahoo Store. En 2001 comenzó a publicar ensayos sobre paulgraham.com, 

que en 2015 obtuvo 34 millones de páginas vistas. En 2005, él y Jessica Livingston, Robert Morris y Trevor Blackwell fundaron

Y Combinator, el primero de un nuevo tipo de incubadora de startups. Desde 2005, Y Combinator ha financiado más de 1000 nuevas 

empresas, incluidas Dropbox, Airbnb, Stripe y Reddit.

[Sam Altman](http://blog.samaltman.com/):

Emprendedor, inversor, programador y blogger estadounidense. Es el presidente de Y Combinator.

[Dustin Moskovitz](https://medium.com/@moskov):

Co-fundador de Facebook y fundador-CEO de Asana.com

[Mark Zuckerberg](https://www.facebook.com/zuck):

Co-fundador de Facebook

[Eric Raymond](http://www.catb.org/):

Informático, escritor y colaborador de software Open Source. Fundó la [Open Source Initiative](https://opensource.org/).
