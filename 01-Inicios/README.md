Inicios
========================

[Ideas para Startups](http://paulgraham.es/ensayos/ideas-para-startups.html), Paul Graham-Versión en Español.

[Consejos para ambiciosos de 19 años](http://blog.samaltman.com/advice-for-ambitious-19-year-olds), Sam Altman-Versión en Ingles.

[Buenas y malas razones para volverse emprendedor](https://medium.com/i-m-h-o/good-and-bad-reasons-to-become-an-entrepreneur-decf0766de8d), Dustin Moskovitz-Versión en Ingles.

