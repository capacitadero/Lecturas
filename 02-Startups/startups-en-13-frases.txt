STARTUPS EN TRECE FRASES
========================
Escrito por Paul Graham.
-----------------------

Febrero de 2009

Una de las cosas que siempre le digo a las startups [a] es un principio que aprendí de Paul Buchheit: 
es mejor hacer realmente felices a unas pocas personas que semi-felices a muchas. Recientemente 
le decía a un reportero que si sólo pudiera decirle a las startups 10 cosas, esta sería una de ellas. 
Entonces pensé: ¿cuales serian las otras 9?

Cuando hice la lista resultó haber 13:

1. Elige buenos cofundadores.

Los cofundadores son a una startup lo que la ubicación es a los bienes raíces. Puedes cambiar todo 
de una casa, excepto el lugar en dónde está. En una startup puedes cambiar tu idea fácilmente, pero 
cambiar a tus cofundadores es difícil. [1] Y el éxito de una startup esta casi siempre en función de
sus fundadores.

2. Lanza rápido.

La razón para lanzar rápido no es tanto que sea crítico poner temprano tu producto en el mercado, 
sino porque en realidad no has empezado a trabajar en él hasta que hayas lanzado. El lanzamiento te 
muestra lo que deberías haber estado construyendo. Hasta que no sepas eso estás perdiendo el tiempo. 
Así que el principal valor de cualquier cosa que lances esta en el pretexto de enrolar usuarios.

3. Deja que tu idea evolucione.

Esta es la segunda parte de lanzar rápido. Lanza rápido y modifica. Es un gran error tratar a una 
startup como si fuera simplemente una cuestión de implementar una idea inicial brillante. Al igual 
que en un ensayo, la mayoría de las ideas aparecen durante la implementación.

4. Entiende a tus usuarios.

Puedes imaginar la riqueza creada por una startup como un rectángulo, donde por un lado está el 
número de usuarios y por el otro la cantidad en que mejoras sus vidas. [2] La segunda dimensión 
es sobre la que tienes mayor control. Y, de hecho, el crecimiento en la primera se verá impulsado 
por la destreza con que manejes la segunda. Al igual que en la ciencia, lo difícil no es responder 
preguntas, sino preguntarlas: lo difícil es ver algo nuevo que los usuarios no tienen. Cuanto mejor 
los entiendas mayores serán las posibilidades de hacerlo. Es por eso que muchas startups exitosas 
hacen algo que los fundadores necesitaban.

5. Mejor hacer que unos pocos usuarios te amen que a muchos ambivalentes.

Lo ideal sería que un gran número de usuarios te ame, pero no puedes esperar llegar a ello de 
inmediato. Inicialmente tienes que elegir entre satisfacer todas las necesidades de un subgrupo 
de usuarios potenciales, o satisfacer una parte de las necesidades de todos los usuarios potenciales. 
Escoge la primera. Es más fácil expandirse hacia una mayor cantidad de usuarios que hacia una mayor 
cantidad de satisfacción. Y quizás lo más importante: es más difícil mentirte a ti mismo. Si crees 
que estás al 85% de un gran producto, ¿cómo sabes que no es el 70%? ¿o el 10%? Mientras que por el 
otro lado es fácil saber cuántos usuarios tienes.

6. Ofrece un servicio al cliente sorprendentemente bueno.

Los clientes están acostumbrados a ser maltratados. La mayoría de las compañías con las que tratan 
son cuasi-monopolios que abusan de una atención al cliente atroz. Tus propias ideas acerca de lo 
que es posible han sido inconscientemente rebajadas por tales experiencias. Trata de hacer tu servicio 
al cliente no sólo bueno, sino sorprendentemente bueno. Inventa maneras de hacer feliz a la gente. 
Estarán anonadados; ya verás. En las primeras etapas de una startup, vale la pena ofrecer un nivel 
de servicio al cliente único, aun si este modelo no es escalable, porque es una manera de aprender 
acerca de tus usuarios.

7. Haces lo que mides.

Esto lo aprendí de Joe Kraus. [3] El sólo hecho de medir algo tiene la misteriosa tendencia de mejorarlo. 
Si quieres hacer que tu número de usuarios suba, pon un gran pedazo de papel en la pared y marca su 
número. Estarás encantado cuando suba y decepcionado cuando baje. Muy pronto comenzarás a notar lo que 
hace que el número suba, y comenzarás a hacer más de eso. Corolario: ten cuidado con lo que mides.

8. Gasta poco.

No puedo enfatizar lo suficiente lo importante que es para una startup controlar sus gastos. La mayoría 
de las startups fracasan antes de hacer algo que la gente quiere, y la forma más común de fracaso es 
quedarse sin dinero. Así que ser barato es (casi) intercambiable con iterar rápidamente. [4] Pero es más 
que eso. Una cultura de lo barato mantiene a las compañías jóvenes de la misma forma en que el ejercicio 
mantiene joven a la gente.

9. Consigue rentabilidad ramen.

"Ramen rentable" [b] significa que una startup gana sólo lo suficiente para pagar los gastos básicos de sus 
fundadores. No es un prototipado rápido para los modelos de negocios (aunque puede ser), sino más bien una 
manera de hackear el proceso de inversión. Una vez que te vuelves ramen rentable, cambia por completo tu 
relación con los inversionistas. También es grandioso para la moral.

10. Evita las distracciones.

Nada mata a las startups como las distracciones. El peor tipo son de las que pagan dinero: trabajos diurnos, 
consultoría y rentables proyectos paralelos. La startup puede tener más potencial a largo plazo, pero siempre
interrumpirás el trabajo en ella para responder a las llamadas de la gente que te esta pagando ahora. 
Paradójicamente, conseguir financiamiento es este tipo de distracción, así que trata de minimizar esto también.

11. No te desmoralices.

Aunque la causa inmediata de muerte en una startup suele ser quedarse sin dinero, la causa subyacente suele 
ser la falta de concentración. O bien la empresa está dirigida por gente estúpida (lo que no se puede solucionar 
con consejos) o la gente es inteligente, pero se desmoralizo. Iniciar una startup es un peso moral enorme. 
Comprende esto y has un esfuerzo consciente para no ser aplastado por ello, del mismo modo que tendrías cuidado 
en doblar las rodillas al levantar una caja pesada.

12. No te rindas.

Incluso si te desmoralizas, no te des por vencido. Puedes llegar sorprendentemente lejos tan sólo no dándote 
por vencido. Esto no es cierto en todos los campos. Hay un gran número de personas que no podrían llegar a ser 
buenos en matemáticas, no importa cuánto tiempo insistieran. Pero las startups no son así. El esfuerzo puro 
suele ser suficiente, siempre y cuando tu idea siga evolucionando.

13. Los negocios se caen.

Una de las habilidades más útiles que aprendimos de Viaweb [c] fue no dejar que nuestras esperanzas se fueran 
muy arriba. Hubo probablemente unos 20 tratos de diferentes tipos que no se concretaron. Después de los primeros 
10 más o menos, aprendimos a tratarlos como procesos de fondo que debíamos ignorar hasta que estuvieran terminados. 
Es muy peligroso para la moral empezar a depender en que los tratos se cierren, no sólo porque a menudo no lo 
hacen, sino porque eso lo hace menos probable.

Habiéndolo resumido en 13 frases, me pregunté a mi mismo con cuál me quedaría si sólo pudiera escoger una.

Entiende a tus usuarios. Esa es la clave. La tarea esencial en una startup es crear riqueza; la dimensión de 
la riqueza sobre la que tienes más control es la cantidad en que puedes mejorar la vida de los usuarios; y la 
parte más difícil de esto es saber qué hacer para ellos. Una vez que sabes qué hacer, hacerlo sólo requiere 
esfuerzo, y cualquier hacker decente es capaz de eso.

Entender a tus usuarios es casi la mitad de los principios en esta lista. Esa es la razón de lanzar temprano, 
para entender a tus usuarios. Dejar que tu idea evolucione es la encarnación de la comprensión de tus usuarios.
Comprender a tus usuarios tenderá a empujarte hacia la creación de algo que haga a unas pocas personas 
profundamente felices. La razón más importante para tener un servicio al cliente sorprendentemente bueno es 
que te ayuda a entender a tus usuarios. Y comprender a tus usuarios servirá incluso de seguro para tu moral, 
porque cuando todo se derrumbe a tu alrededor, tener aunque sea sólo a diez usuarios que te aman te permitirá 
seguir adelante.

Notas

[1] Estrictamente hablando, es imposible sin una máquina del tiempo.

[2] En la práctica, es más como un peine irregular.

[3] Joe cree que uno de los fundadores de Hewlett Packard lo dijo primero, pero no recuerda cuál.

[4] Serían intercambiables si los mercados permanecieran inmóviles. Como no es así, trabajar el doble de 
rápido es mejor que tener el doble de tiempo.

Notas del Traductor

[a] El termino startup define a una empresa de reciente creación orientada a la tecnología. El mismo Paul Graham 
la define a la perfección en Como Financiar una Startup: "Una empresa tiene que ser más que pequeña y de reciente 
creación para ser una startup. Hay millones de pequeñas empresas en Estados Unidos, pero sólo unas pocas miles son 
startups. Para ser una startup, una compañía tiene que ser un negocio de productos, no un negocio de servicios. 
Lo que no quiere decir que tiene que hacer algo físico, sino que tiene que tener una cosa que vende a mucha gente,
en vez de hacer trabajos a medida para clientes individuales. El trabajo sobre encargo no es escalable. Para ser una 
startup tienes que ser la banda que vende un millón de copias de una canción, no el grupo que gana dinero tocando en 
bodas y Bar Mitzvahs individuales."

[b] Ramen es una marca de sopa instantanea muy popular en Estados Unidos, del tipo a la que sólo hay que agregarle 
un poco de agua caliente y esperar tres minutos para consumirla.

[c] Viaweb era una empresa que propocionaba software y herramientas de informes para construir y hacer funcionar 
sitios de comercio electrónico en la Web. Su producto de comercio electrónico se implementó inicialmente en lenguaje 
Common Lisp.
La empresa fue fundada por Paul Graham yRobert Tappan Morris en 1995. Fue adquirida en el verano de 1998 por Yahoo!, mediante un intercambio de acciones equivalente a unos 49 millones de dólares. En Yahoo!, el producto se denominó Yahoo! Store. [Fuente: Wikipedia]