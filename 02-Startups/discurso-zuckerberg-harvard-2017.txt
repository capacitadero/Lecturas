DISCURSO DE CEREMONIA DE GRADUACIÓN- UNIVERSIDAD DE HARVARD 2017
================================================================
Escrito por Mark Zuckerberg.
---------------------------
Mayo 2017

Rectora Faust, junta de supervisión, profesores, exalumnos, amigos, padres orgullosos, 
miembros de la junta asesora y graduados de la mejor universidad del mundo.

Para mí es un honor estar hoy aquí, porque, digámoslo, han conseguido algo que yo 
nunca pude. Si sobrevivo a este discurso, será la primera vez que termine algo en 
Harvard. ¡Enhorabuena, promoción del 2017!

Se me hace raro estar aquí, no solo porque dejara la universidad, sino también porque, 
técnicamente, somos de la misma generación. Hace menos de una década, yo estudiaba
las mismas ideas y me quedaba dormido en las mismas clases. Puede que hayamos tomado 
distintos caminos para llegar aquí, especialmente si venís desde el Quad, pero hoy 
quiero hablar de lo que he aprendido de nuestra generación y del mundo en que vivimos.

Estos dos últimos días me han traído muy buenos recuerdos.

¿Cuántos recuerdan que estaban haciendo cuando recibieron el email de admisión en 
Harvard? Yo estaba jugando al Civilization. Bajé corriendo hasta donde estaba mi padre, 
cuya reacción fue grabarme mientras abría el email. Podría haber sido un vídeo muy 
triste. Os juro que entrar en Harvard sigue siendo de lo que más orgullosos están mis 
padres.

Motivación es pensar que formamos parte de algo más grande, que somos necesarios, que 
tenemos un objetivo por el que trabajar. La motivación es lo que nos hace realmente 
felices.

En mis viajes, he charlado con niños en centros de menores y con drogadictos, que me 
han contado que su vida podría haber sido diferente de haber tenido algo que hacer 
después de clase. He conocido a obreros que saben que no recuperarán su antiguo trabajo 
y tratan de encontrar su lugar.

Para que nuestra sociedad siga avanzando, como generación debemos crear no solo nuevos 
empleos, sino también nuevas motivaciones.

Recuerdo la noche en la que lancé Facebook desde mi dormitorio en Kirkland House. Fui
a Noch's con mi amigo KX. Recuerdo decirle que me hacía mucha ilusión conectar a la 
comunidad de Harvard, pero que un día alguien conectaría todo el mundo.

Nunca pensé que ese alguien seríamos nosotros. Solo éramos compañeros de universidad. 
No teníamos ni idea de cómo hacerlo. Había grandes empresas tecnológicas con recursos, 
por lo que daba por sentado que una de ellas lo haría. Pero una cosa teníamos clara: 
todo el mundo quiere estar conectado. Así que seguimos trabajando, día a día.

Seguro que a muchos de vosotros os ha pasado lo mismo. Un cambio que parece tan obvio 
que estás seguro de que otros lo harán. Pero no serán otros, sino vosotros.

Sin embargo, no basta con estar motivado. También hay que motivar a los demás.

Es algo que descubrí por las malas. Mi esperanza no fue nunca crear una empresa, sino 
generar impacto. Cuando se nos unieron todas estas personas, supuse que ese era también 
su objetivo, por lo que no dije nada.

No basta con estar motivado. También hay que motivar a los demás.

Un par de años después, nos quisieron comprar grandes empresas, pero yo no quise vender. 
Quería comprobar si podíamos conectar a más personas. Estábamos creando la primera 
sección de noticias y pensé que, si la lanzábamos, cambiaríamos la forma de conocer 
el mundo.

Casi todos los demás querían vender. Sin una motivación mayor, era el sueño de cualquier 
startup. Esta situación dividió a nuestra empresa. Tras una fuerte discusión, un asesor 
me dijo que, si no accedía a vender, lo lamentaría el resto de mi vida. La crispación era 
tal que, en un año, todo el equipo de dirección acabó marchándose.

Fue mi peor momento al frente de Facebook. Creía en lo que hacíamos, pero me sentía solo. 
Y lo que es peor, yo era el culpable. Me preguntaba si estaba equivocado, si era un chaval
de 22 años que no sabía cómo funcionaba el mundo.

Ahora, años después, sé que así es como funcionan las cosas sin una motivación mayor. Somos 
nosotros los que debemos generarla para poder seguir avanzando juntos.

Hoy quiero hablaros de tres formas de crear un mundo donde todos tengan una motivación: 
asumiendo juntos grandes proyectos relevantes, redefiniendo la igualdad de oportunidades 
para tener la libertad de elegir nuestra motivación y creando una comunidad mundia

Primero, debemos asumir grandes proyectos relevantes.

Nuestra generación deberá convivir con la sustitución de decenas de millones de empleos 
por coches y camiones autónomos. Sin embargo, tenemos potencial para hacer mucho más juntos.

Cada generación tiene obras que la definen. Más de 300 000 personas trabajaron para 
enviar al hombre a la Luna, incluido ese conserje. Millones de voluntarios inmunizaron 
a niños de todo el mundo contra la polio. Otros tantos construyeron la presa Hoover y 
otros grandes proyectos.Estos proyectos no solo sirvieron de motivación a quienes 
participaron en ellos; también demostró al país que podíamos hacer grandes cosas.

Ahora, nos toca a nosotros hacer grandes cosas. Es probable que estan pensando: "No sé 
cómo construir una presa ni involucrar a millones de personas en algo".

Dejen que les cuente un secreto: nadie sabe al principio. Las ideas tienen que ir tomando 
forma. Se van haciendo realidad al trabajar en ellas. Solo hay que ponerse en marcha.

Si hubiese tenido que saber de antemano todo sobre cómo conectar a personas, nunca 
hubiese puesto en marcha Facebook.

El cine y la cultura pop están muy equivocados. Esa idea de un momento de brillantez 
es una mentira peligrosa. Nos hace sentir ineptos por no tener el nuestro. Impide que 
gente con el germen de buenas ideas las pongan en marcha. ¿Saben en qué más se equivoca 
el cine? Nadie escribe fórmulas matemáticas sobre cristal. Eso no existe.

Lo cierto es que todo lo que hagamos planteará problemas en el futuro, pero eso no debería 
echarnos atrás.

Está bien ser idealista, pero preparaos para ser unos incomprendidos. Trabajad en una 
gran idea y os llamarán locos, aunque al final salga bien. Trabajad en un problema complejo 
y os culparán por no llegar a comprender el desafío, aunque sea imposible saberlo todo de 
antemano. Tomad la iniciativa y os criticarán por ir demasiado rápido, porque siempre habrá 
quien os quiera ralentizar.

A menudo, nuestra sociedad no hace grandes cosas porque tiene tanto miedo a equivocarse que 
acaba por ignorar qué está mal hoy en día. Lo cierto es que todo lo que hagamos planteará 
problemas en el futuro, pero eso no debería echarnos atrás.

Así que ¿a qué estamos esperando? Es hora de que nuestra generación haga las obras públicas 
que la definan. ¿Y si detenemos el cambio climático antes de que destruyamos el planeta y 
hacemos que millones de personas fabriquen e instalen paneles solares? ¿Y si curamos todas 
las enfermedades y pedimos voluntarios para rastrear su historial clínico y compartir su genoma? 
Hoy en día gastamos 50 veces más en tratar a los enfermos que en buscar curas para que la 
gente no enferme. No tiene sentido. Podemos solucionarlo. ¿Y si modernizamos la democracia 
para que todos puedan votar en internet y personalizamos la educación para que todos puedan 
aprender?

Son logros que están a nuestro alcance. Hagámoslo de tal modo que involucremos a toda 
la sociedad. Hagamos grandes cosas. No generemos solo progreso, también motivación.

Si queremos crear un mundo donde todos tengamos una motivación, lo primero que debemos 
hacer es asumir proyectos importantes.

Lo segundo es establecer igualdad de oportunidades para que todos dispongamos de los medios 
para poder alcanzar nuestras metas.

Muchos de nuestros padres han tenido trabajos estables a lo largo de sus carreras profesionales. 
Pero hoy en día todos somos emprendedores, tanto si estamos iniciando un proyecto como si 
estamos buscando trabajo. Y eso es genial, porque, gracias a ese espíritu emprendedor, 
estamos progresando mucho.

La sociedad en la que vivimos se preocupa demasiado por recompensar el éxito y no nos esforzamos 
lo suficiente para ayudar a los demás a intentar triunfar.

El espíritu emprendedor prospera cuando es fácil probar muchísimas ideas diferentes. Facebook 
o fue el primer proyecto que desarrollé. También desarrollé juegos, sistemas de mensajería 
instantánea, herramientas para estudiar y reproductores de música. Y no soy el único. A JK Rowling 
la rechazaron 12 veces antes de publicar Harry Potter. Incluso Beyoncé tuvo que componer cientos 
de canciones hasta conseguir Halo. Los éxitos más importantes se consiguen cuando existe la 
posibilidad de fracasar.

Pero hoy en día, existe un nivel tan alto de desigualdad que todos nos vemos afectados. Cuando 
alguien no dispone de los medios para convertir su idea en una iniciativa histórica, todos salimos
perdiendo. En la actualidad, la sociedad en la que vivimos se preocupa demasiado por recompensar 
el éxito y no nos esforzamos lo suficiente para ayudar a los demás a intentar triunfar.

Seamos realistas. Hay algo que no funciona en nuestro sistema, ya que algunos de nosotros 
podemos hacer muchas cosas mientras que millones de estudiantes no pueden pagar los préstamos 
universitarios y, mucho menos, la creación de una empresa.

Conozco a muchos emprendedores y ninguno ha tirado la toalla al ver que la empresa que está 
creando posiblemente no sea rentable. Sin embargo, conozco a muchas personas que no han cumplido 
sus sueños porque no contaban con un colchón en el que caer si fracasaban.

Todos sabemos que el éxito no llega solo por tener una idea brillante o por trabajar mucho. 
También hay que tener suerte. Si hubiera tenido que ayudar a mi familia a salir adelante en 
lugar de tener tiempo para programar; si no hubiera sabido que no pasaría nada si Facebook no 
funcionaba, hoy no estaría aquí. Sinceramente, todos sabemos la suerte que hemos tenido.

Todas las generaciones amplían su definición de igualdad. Las generaciones anteriores lucharon 
por el derecho a votar y los derechos civiles. Vivieron el Nuevo Trato y la Gran Sociedad. 
Este es el momento de definir un nuevo compromiso social para nuestra generación.

Debemos medir el progreso, no solo mediante datos económicos como el PIB, sino también por 
el número de personas que consideran que tienen una labor importante.

Debemos medir el progreso, no solo mediante datos económicos como el PIB, sino también por 
el número de personas que consideran que tienen una labor importante. Debemos desarrollar 
ideas como la renta básica universal con el objetivo de ofrecer a todas las personas un 
colchón para poder emprender nuevos proyectos. Seguramente cambiemos muchas veces de trabajo, 
por lo que necesitamos una atención sanitaria e infantil accesibles que no dependan de 
ninguna empresa. Seguramente cometamos muchos errores, por lo que necesitamos una sociedad 
que no se centre tanto en castigarnos o estigmatizarnos. Y, como la tecnología no deja de 
avanzar, debemos centrarnos más en seguir formándonos a lo largo de nuestra vida.

Y sí, disponer de los medios para alcanzar nuestras metas no es gratuito. Personas como yo 
debemos pagar por ellos. Muchos de vosotros conseguirán grandes cosas y tendrán que pagar 
igualmente .

Esta es la razón por la que Priscilla y yo creamos la Chan Zuckerberg Initiative y destinamos 
todos nuestros recursos en fomentar la igualdad de oportunidades. Estos son los valores de 
nuestra generación. Nunca dudamos en llevarla a cabo. La única duda que tuvimos era cuándo hacerlo.

La generación Y es una de las generaciones más solidarias de todos los tiempos. En un año, 
tres de cada cuatro personas de esta generación en EE. UU. ha hecho una donación, y siete de 
cada diez ha recaudado dinero.

Pero el dinero no lo es todo. También puedes ofrecer tiempo. Os prometo que, con solo un par 
de horas a la semana, podrán ayudar a alguien a sacar todo su potencial.

Seguro piensan que es mucho tiempo. Yo, al menos, lo pensaba. Cuando Priscilla se graduó en Harvard, 
se hizo profesora. Pero antes de trabajar conmigo en el sector educativo, me dijo que debía impartir 
clases. Yo le dije: "Bueno, estoy bastante ocupado. Estoy llevando esta empresa". Pero ella insistió 
y, al final, impartí un curso de enseñanza secundaria sobre iniciativa empresarial en el Boys and 
girls club local.

Les di clases sobre desarrollo de producto y marketing, y ellos me enseñaron lo que se siente cuando 
te señalan con el dedo por tu raza o por tener a un familiar en la cárcel. Yo compartía historias 
de mi época estudiantil y ellos compartían conmigo su ilusión por ir a la universidad. Desde hace 
cinco años, voy a comer con esos niños una vez al mes. Incluso uno de ellos nos organizó nuestra 
primera fiesta de nacimiento. Y el año que viene empiezan la universidad. Todos. Los primeros en 
sus familias.

Todos podemos sacar tiempo para echar una mano a alguien. Todos deberíamos contar con los medios 
para poder alcanzar nuestros objetivos, no solo porque es lo que hay que hacer, sino también 
porque cuantas más personas puedan elegir su motivación, mayores serán los beneficios para todos.

La motivación no nace solo del trabajo. La tercera manera de hacer que todos tengamos una 
motivación es construir una comunidad. Cuando nuestra generación habla de "todos", se refiere 
a "todas las personas del mundo".

Levantad un momento la mano. ¿Cuántos de vosotros son de otro país? Y ahora, ¿cuántos son amigos 
de ellos? Ahora nos entendemos. Hemos crecido conectados.

En una encuesta a personas de todo el mundo pertenecientes a la generación Y, se les preguntaba 
por aquello que definía su identidad. La respuesta más extendida no fue la nacionalidad, la 
religión o la etnia, sino "Soy ciudadano del mundo". Esto es muy importante.

Cada generación amplía el circulo de las personas que considera "uno de los nuestros". 
Para nosotros, ese círculo engloba a todo el planeta.

Sabemos que durante la historia de la humanidad, nos rendimos ante las numerosas personas que 
se unen (desde las tribus hasta las ciudades y las naciones) para conseguir todo aquello que 
no podrían conseguir por sí solos.

Para progresar en la actualidad, debemos unirnos, no solo a nivel local o nacional, sino a 
nivel global.

Somos conscientes de que las grandes oportunidades ahora son globales. Podemos ser la generación 
que acabe con la pobreza y las enfermedades. Somos conscientes de que los mayores desafíos 
precisan respuestas también globales. Ningún país puede luchar contra el cambio climático o 
prevenir pandemias en solitario. Para progresar en la actualidad, debemos unirnos, no solo a 
nivel local o nacional, sino a nivel global.

De todos modos, vivimos en una época inestable. La globalización se está olvidando de muchas 
personas en todo el mundo. Es muy difícil preocuparnos por personas de otros lugares del mundo 
si no estamos satisfechos con nuestras vidas. Prima más todo aquello que nos rodea.

Esta es la batalla que nos ha tocado librar: el poder de la libertad, la amplitud de miras y 
la comunidad global contra el poder del autoritarismo, el aislacionismo y el nacionalismo. 
El poder del flujo de la información, el comercio y la inmigración contra aquellos que quieren 
frenarlos. Esta no es una lucha de naciones, es una lucha de ideas. En todos los países hay 
personas que apoyan la conexión global y otras que están en contra.

Pero la decisión no está en manos de las Naciones Unidas. Esta decisión se tomará a nivel local, 
cuando haya un gran número de personas que tengan una motivación y la estabilidad suficiente 
para abrir la mente y preocuparnos por los demás. La mejor manera de hacer esto es empezar a 
crear comunidades locales.

Nuestras comunidades dan sentido a todo. Tanto si nuestras comunidades son casas o equipos 
deportivos como iglesias o grupos de música, nos hacen sentir que formamos parte de algo grande, 
que no estamos solos. Nos dan la fuerza necesaria para ampliar nuestros horizontes.

Por eso sorprende que, durante décadas, los miembros de cualquier tipo de grupo han disminuido 
más del 25 %. Ese 25 % está compuesto por muchas personas que ahora tienen que marcarse objetivos 
en otros lugares.

Pero estoy convencido de que podemos volver a reconstruir estas comunidades y crear otras, 
porque muchos de vosotros ya lo estan haciendo.

He conocido a Agnes Igoye, que se gradúa hoy. ¿Dónde estás, Agnes? Ha vivido durante toda su 
infancia en zonas en conflicto en Uganda. Ahora, enseña a miles de agentes de policía a mantener 
la seguridad de las comunidades.

He conocido a Kayla Oakley y Niha Jain, que también se gradúan hoy. Levantaos. Kayla y Niha 
crearon una organización sin ánimo de lucro que conecta a las personas que sufren enfermedades 
con personas de sus comunidades dispuestas a ayudar.

He conocido a David Razu Aznar, que se gradúa hoy en la Kennedy School. David, levántate. 
Es un antiguo concejal que ha liderado la lucha por el matrimonio entre personas del mismo 
sexo en Ciudad de México, haciendo de esta ciudad la primera de Latinoamérica en aprobarlo, 
incluso antes que San Francisco.

Esta es mi historia. Un estudiante en una habitación que conecta comunidades una por una y 
que seguirá insistiendo hasta que un día el mundo entero esté conectado. El cambio empieza a 
nivel local. Incluso los cambios globales empiezan a pequeña escala y con personas como nosotros. 
En nuestra generación, el debate de si nos conectamos más, de si conseguimos las mejores oportunidades 
depende de vuestra capacidad de crear comunidades y construir un mundo en el todas las personas 
tengan motivaciones.

Promoción de 2017, se estan graduando en un mundo que necesita objetivos. Está en vuestras manos 
el crearlos. Seguro que estan pensando: "¿Realmente soy capaz de hacerlo?"

¿Recuerdan de lo que les he dicho sobre la clase que di en el Boys and girls club? Un día después de 
clase, les estaba hablando de la universidad y uno de mis mejores estudiantes alzó la mano y dijo que 
no sabía si podría ir porque no tenía papeles. Que no sabía si le dejarían ir.

El año pasado, me lo llevé a desayunar por su cumpleaños. Quería hacerle un regalo, así que le pregunté 
y empezó a hablar sobre los estudiantes que lo pasaban mal y, al final, me dijo: "Lo único que quiero 
es un libro sobre justicia social".

Me impactó muchísimo. Un chaval que tiene todas los motivos para ser cínico. No sabía si el país al 
que él llama hogar (y el único que ha conocido) le negaría la oportunidad de cumplir su sueño de ir a 
la universidad. Pero no se sentía mal por él. Ni siquiera pensaba en él. Él tiene una gran motivación 
en la vida y servirá de ejemplo a muchas personas.

Que el poder de la fuerza, que ha bendecido a los que vinieron antes que nosotros, nos ayude a encontrar 
la valentía para hacer de nuestras vidas una bendición".

No puedo ni decir su nombre para no ponerle en peligro, hecho que refleja la situación actual. Pero, 
si un estudiante de último año que no sabe qué le deparará el futuro puede aportar su granito de arena 
para que el mundo siga adelante, los demás también podemos hacerlo, se lo debemos al mundo.

Antes de que crucen esa puerta por última vez y aprovechando que estamos frente a la Memorial Church, 
os recitaré Mi Shrebeirach, una oración que he recordado, que repito cuando me enfrento a un reto y 
que canto a mi hija pensando en su futuro cuando la meto en la cama. Dice así:

"Que el poder de la fuerza, que ha bendecido a los que vinieron antes que nosotros, nos ayude a encontrar 
la valentía para hacer de nuestras vidas una bendición".

Espero que encuentren la valentía y hagan de vuestras vidas una bendición.

Enhorabuena, promoción del 2017. Que tengan muchísima suerte!