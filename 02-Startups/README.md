Startups
========

[Cómo son en realidad las startups](http://paulgraham.es/ensayos/como-son-en-realidad-las-startups.html), Paul Graham-Versión en Español.

[Startups en 13 frases](http://paulgraham.es/ensayos/startups-en-13-frases.html), Paul Graham-Versión en Español.

[Discurso de graduación en Harvard 2017](https://www.youtube.com/watch?v=XKV5XctH6_Q), Mark Zuckerberg-Video.



