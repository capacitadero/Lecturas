Hackers
=======

[Cómo hacer preguntas de manera inteligente?](http://www.catb.org/esr/faqs/smart-questions.html), Eric Raymond-Versión en Ingles.

[Hackers y Pintores](http://paulgraham.es/ensayos/hackers-y-pintores.html), Paul Graham-Versión en Español.
